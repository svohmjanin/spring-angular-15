import {IGenericObject, JsonProperty} from 'json-typescript-mapper';

export class AuthorModel implements IGenericObject {
  @JsonProperty('id')
  id: number = null;
  @JsonProperty('name')
  name: string = null;
}
