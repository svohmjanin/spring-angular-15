import {IGenericObject, JsonProperty} from 'json-typescript-mapper';

export class CommentModel implements IGenericObject {
  @JsonProperty('id')
  id: number = null;
  @JsonProperty('name')
  name: string = null;
  @JsonProperty('content')
  content: string = null;
}
