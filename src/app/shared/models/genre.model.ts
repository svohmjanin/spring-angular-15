import {IGenericObject, JsonProperty} from 'json-typescript-mapper';

export class GenreModel implements IGenericObject {
  @JsonProperty('id')
  id: number = null;
  @JsonProperty('name')
  name: string = null;
}
