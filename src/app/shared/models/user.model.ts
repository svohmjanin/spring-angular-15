import {IGenericObject, JsonProperty} from 'json-typescript-mapper';

export class User implements IGenericObject{
  @JsonProperty('username')
  username: string = null;
  @JsonProperty('password')
  password: string = null;
  @JsonProperty('first_name')
  firstName: string = null;
  @JsonProperty('last_name')
  lastName: string = null;
}
