import {IGenericObject, JsonProperty} from 'json-typescript-mapper';
import {AuthorModel} from './author.model';
import {GenreModel} from './genre.model';
import {CommentModel} from './comment.model';

export class BookModel implements IGenericObject {
  @JsonProperty('id')
  id: number = null;
  @JsonProperty({clazz: AuthorModel, name: 'author'})
  author: AuthorModel = null;
  @JsonProperty({clazz: GenreModel, name: 'genre'})
  genre: GenreModel = null;
  @JsonProperty('name')
  name: string = null;
  @JsonProperty({clazz: CommentModel})
  comments: CommentModel[] = null;
}
