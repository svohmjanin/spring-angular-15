import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {AuthenticationService} from '../services/authentication.service';
import {Observable} from 'rxjs';
import {isNull} from 'util';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthenticationService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!isNull(this.auth.getToken())){
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer_${this.auth.getToken()}`
        }
      });
    }
    return next.handle(request);
  }
}
