import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {isNull} from 'util';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    return this.http.post<any>('/api/v1/auth/login', { username, password })
      .pipe(map(user => {
        if (user && user.token) {
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

  registration(username: string, password: string, firstName: string, lastName: string) {
    return this.http.post<any>('/api/v1/auth/registration', { username, password, firstName, lastName }).pipe(map(user => {
      if (user && user.token) {
        localStorage.setItem('currentUser', JSON.stringify(user));
      }
      return user;
    }));
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  getToken(): string {
    const currentUser = JSON.parse( localStorage.getItem('currentUser'));

    if (isNull(currentUser)) { return null; }

    return currentUser.token;
  }

}
