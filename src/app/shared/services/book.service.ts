import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable, of, throwError} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';

import {sprintf} from 'sprintf-js';
import {BookModel} from '../models/book.model';
import {deserialize} from 'json-typescript-mapper';
import {CommentModel} from '../models/comment.model';


@Injectable()
export class BookService {
  constructor(private http: HttpClient) {
  }

  private inProcessSubject$ = new BehaviorSubject<boolean>(false);
  inProgress$ = this.inProcessSubject$.asObservable();

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getBooks(): Observable<BookModel[]> {

    this.inProcessSubject$.next(true);

    return this.http.get<BookModel[]>('/api/v1/book');
  }

  getBook(id): Observable<BookModel> {
    return this.http.get<BookModel>(sprintf('/api/v1/book/%d', id));
  }

  getComments(id): Observable<CommentModel[]> {
    return this.http.get<CommentModel[]>(sprintf('/api/v1/book/%d/comments', id));
  }

  createBook(book): Observable<BookModel> {
    return this.http.post<BookModel>('/api/v1/book', JSON.stringify(book), this.httpOptions);
  }

  updateBook(id, employee): Observable<BookModel> {
    return this.http.put<BookModel>('/api/v1/book', JSON.stringify(employee), this.httpOptions);
  }

  deleteBook(id) {
    return this.http.delete<BookModel>(sprintf('/api/v1/book/%d', id), this.httpOptions);
  }
}
