import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeAppComponent} from './main/home-app/home-app.component';
import {BookIndexComponent} from './main/book/index/book-index.component';
import {BookEditComponent} from './main/book/edit/book-edit.component';
import {BookShowComponent} from './main/book/show/book-show.component';
import {NotFoundAppComponent} from './main/not-found-app/not-found-app.component';
import {BookCreateComponent} from './main/book/create/book-create.component';
import {LoginComponent} from './main/login/login.component';
import {AuthGuard} from './shared/guards/auth.guard';
import {RegistrationComponent} from './main/registration/registration.component';


const routes: Routes = [
  {path: '', component: HomeAppComponent},
  {path: 'login', component: LoginComponent },
  {path: 'registration', component: RegistrationComponent },
  {path: 'book', component: BookIndexComponent, canActivate: [AuthGuard]},
  {path: 'book-show/:id', component: BookShowComponent, canActivate: [AuthGuard]},
  {path: 'book-edit/:id', component: BookEditComponent, canActivate: [AuthGuard]},
  {path: 'book-create', component: BookCreateComponent, canActivate: [AuthGuard]},
{path: '**', component: NotFoundAppComponent}

];

@NgModule({
imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
