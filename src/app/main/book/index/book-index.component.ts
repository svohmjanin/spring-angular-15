import {Component, OnInit} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {BookService} from '../../../shared/services/book.service';
import {BookModel} from '../../../shared/models/book.model';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-book-index',
  templateUrl: './book-index.component.html',
})
export class BookIndexComponent implements OnInit {

  books: BookModel[] = [];
  private inProcessSubject$ = new BehaviorSubject<boolean>(false);
  inProgress$ = this.inProcessSubject$.asObservable();

  constructor(
    public bookService: BookService
  ) { }

  ngOnInit() {
    this.loadBooks();
  }

  loadBooks() {
    this.inProcessSubject$.next(true);

    return this.bookService.getBooks().subscribe((data: BookModel[]) => {
      this.books = data;
      this.inProcessSubject$.next(false);
      console.log(this.books);
    });
  }

  deleteBook(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.bookService.deleteBook(id).subscribe(data => {
        this.loadBooks();
      });
    }
  }

}
