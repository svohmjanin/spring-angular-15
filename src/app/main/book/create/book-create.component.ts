import {Component, Input, OnInit} from '@angular/core';
import {BookService} from '../../../shared/services/book.service';
import {Router} from '@angular/router';
import {AuthorService} from '../../../shared/services/author.service';
import {GenreService} from '../../../shared/services/genre.service';
import {BehaviorSubject, forkJoin} from 'rxjs';
import {AuthorModel} from '../../../shared/models/author.model';
import {GenreModel} from '../../../shared/models/genre.model';
import {BookModel} from '../../../shared/models/book.model';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html'
})
export class BookCreateComponent implements OnInit {

  authors: AuthorModel[] = [];
  genres: GenreModel[] = [];
  private inProcessSubject$ = new BehaviorSubject<boolean>(false);
  inProgress$ = this.inProcessSubject$.asObservable();

  @Input() bookData = null;

  constructor(
    public bookService: BookService,
    public authorService: AuthorService,
    public genreService: GenreService,
    public router: Router
  ) {
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.inProcessSubject$.next(true);

    forkJoin([
      this.genreService.getGenres(),
      this.authorService.getAuthors()
    ]).subscribe(results => {
      this.genres = results[0];
      this.authors = results[1];
      this.bookData = {name: '', author: {}, genre: {}};
      this.inProcessSubject$.next(false);
      console.log(this.bookData);
    });
  }

  addBook() {
    this.bookService.createBook(this.bookData).subscribe((data: BookModel) => {
      this.router.navigate(['/book']);
    });
  }

}
