import { Component, OnInit } from '@angular/core';
import {BookModel} from '../../../shared/models/book.model';
import {BookService} from '../../../shared/services/book.service';
import {AuthorService} from '../../../shared/services/author.service';
import {GenreService} from '../../../shared/services/genre.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BehaviorSubject, forkJoin} from 'rxjs';
import {CommentModel} from '../../../shared/models/comment.model';
import {GenreModel} from '../../../shared/models/genre.model';
import {AuthorModel} from '../../../shared/models/author.model';

@Component({
  selector: 'app-show',
  templateUrl: './book-show.component.html',
})
export class BookShowComponent implements OnInit {

  id = this.actRoute.snapshot.params.id;
  bookData: BookModel = null;
  authors: AuthorModel[] = [];
  genres: GenreModel[] = [];
  comments: CommentModel[] = [];
  private inProcessSubject$ = new BehaviorSubject<boolean>(false);
  inProgress$ = this.inProcessSubject$.asObservable();


  constructor(
    public bookService: BookService,
    public authorService: AuthorService,
    public genreService: GenreService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.inProcessSubject$.next(true);

    forkJoin([
      this.genreService.getGenres(),
      this.authorService.getAuthors(),
      this.bookService.getBook(this.id),
      this.bookService.getComments(this.id)
    ]).subscribe(results => {
      this.genres = results[0];
      this.authors = results[1];
      this.bookData = results[2];
      this.comments = results[3];
      this.inProcessSubject$.next(false);
      console.log(this.bookData);
    });
  }

}
