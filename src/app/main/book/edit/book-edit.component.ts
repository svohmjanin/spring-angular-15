import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BookService} from '../../../shared/services/book.service';
import {AuthorService} from '../../../shared/services/author.service';
import {GenreService} from '../../../shared/services/genre.service';
import {BookModel} from '../../../shared/models/book.model';
import {AuthorModel} from '../../../shared/models/author.model';
import {GenreModel} from '../../../shared/models/genre.model';
import {isNull} from 'util';
import {BehaviorSubject, forkJoin} from 'rxjs';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
})
export class BookEditComponent implements OnInit {

  id = this.actRoute.snapshot.params.id;
  bookData: BookModel = null;
  authors: AuthorModel[] = [];
  genres: GenreModel[] = [];
  private inProcessSubject$ = new BehaviorSubject<boolean>(false);
  inProgress$ = this.inProcessSubject$.asObservable();

  constructor(
    public bookService: BookService,
    public authorService: AuthorService,
    public genreService: GenreService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) {
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.inProcessSubject$.next(true);
    console.log(this.inProgress$);

    forkJoin([
      this.genreService.getGenres(),
      this.authorService.getAuthors(),
      this.bookService.getBook(this.id)
    ]).subscribe(results => {
      this.genres = results[0];
      this.authors = results[1];
      this.bookData = results[2];
      this.inProcessSubject$.next(false);
      console.log(this.bookData);
      console.log(this.inProgress$);
    });
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  updateBook() {
    if (window.confirm('Are you sure, you want to update?')) {
      this.bookService.updateBook(this.id, this.bookData).subscribe(data => {
        this.router.navigate(['/book']);
      });
    }
  }

}
