import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BookService} from './shared/services/book.service';
import {AuthorService} from './shared/services/author.service';
import {GenreService} from './shared/services/genre.service';
import {NotFoundAppComponent} from './main/not-found-app/not-found-app.component';
import {BookIndexComponent} from './main/book/index/book-index.component';
import {HomeAppComponent} from './main/home-app/home-app.component';
import {BookShowComponent} from './main/book/show/book-show.component';
import {BookEditComponent} from './main/book/edit/book-edit.component';
import {BookCreateComponent} from './main/book/create/book-create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './main/login/login.component';
import {AuthGuard} from './shared/guards/auth.guard';
import {AuthenticationService} from './shared/services/authentication.service';
import { RegistrationComponent } from './main/registration/registration.component';
import {TokenInterceptor} from './shared/guards/token.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundAppComponent,
    HomeAppComponent,
    BookIndexComponent,
    BookShowComponent,
    BookEditComponent,
    BookCreateComponent,
    LoginComponent,
    RegistrationComponent
],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
providers: [
  BookService,
  AuthorService,
  GenreService,
  AuthGuard,
  AuthenticationService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
],
bootstrap: [AppComponent]
})
export class AppModule { }
